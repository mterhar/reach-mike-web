# Mike's Web3 Contact Form Web Interface

Want to get ahold of Mike, reach out to him this way.

See the [ethereum contract here](gitlab.com/mterhar/reach-mike-eth).

## Deploys to a Web host

This is a solidity project that deploys to Ethereum blockchain. 

## Usage

A web application presents the users with the ability to connect their wallet (for identity) and then communicate with me. 

### First version is super simple and public

## Roadmap

1. Make it serverless or client-only
2. ???
