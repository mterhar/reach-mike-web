FROM node:14.17-alpine3.12

# Uncomment if use of `process.dlopen` is necessary
# apk add --no-cache libc6-compat

ENV PORT 3000
EXPOSE 3000

ARG NODE_ENV=production
ENV NODE_ENV $NODE_ENV

WORKDIR /app
COPY package.json yarn.lock ./
RUN yarn install --production
COPY . .

CMD ["yarn", "start"]